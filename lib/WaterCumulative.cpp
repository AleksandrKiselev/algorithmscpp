#include "WaterCumulative.h"
#include <stack>
#include <algorithm>

int WaterCumulative(const std::vector<int> heights)
{
	int result = 0;

	struct frame { int idx; int height; };
	std::stack<frame> stack;

	for (int curr_idx = 0; curr_idx < heights.size() - 1; ++curr_idx)
	{
		const int curr_height = heights[curr_idx];
		const int next_idx = curr_idx + 1;
		const int next_height = heights[next_idx];
		
		if (curr_height > next_height)
		{
			int temp = curr_height;
			while (temp != next_height)
			{
				stack.push({ curr_idx, temp });
				temp--;
			}
			continue;
		}

		if (curr_height == next_height)
		{
			continue;
		}

		if (curr_height < next_height)
		{
			while (!stack.empty())
			{
				auto& top = stack.top();
				if (next_height < top.height) break;
				result += curr_idx - top.idx;
				stack.pop();
			}
			continue;
		}
	}

	return result;
}