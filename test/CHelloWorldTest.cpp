#include "pch.h"
#include "../lib/CHelloWorld.h"

TEST(CHelloWorldTest, Say) {
	CHelloWorld hw;
	const std::string expected = "Hello World!";
	ASSERT_EQ(hw.Say(), expected);
}