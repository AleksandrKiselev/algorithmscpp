#include "pch.h"
#include "../lib/WaterCumulative.h"

TEST(WaterCumulativeTest, Flat) {
	/* _______________  */
	const std::vector<int> heights(15, 0);
	const std::size_t expected = 0;
	ASSERT_EQ(WaterCumulative(heights), expected);
}

TEST(WaterCumulativeTest, Simple) {
	/*
	     _         _
	   _| |_    __| |_
	 _|     |__|      |_  
	 
	*/
	const std::vector<int> heights = { 0, 1, 2, 1, 0, 0, 1, 1, 2, 1, 0 };
	const std::size_t expected = 7;
	ASSERT_EQ(WaterCumulative(heights), expected);
}

TEST(WaterCumulativeTest, StairUp) {
	/*
	        _
	      _|
	    _|
	  _|
	_|

	*/
	const std::vector<int> heights = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	const std::size_t expected = 0;
	ASSERT_EQ(WaterCumulative(heights), expected);
}

TEST(WaterCumulativeTest, StairDown) {
	/*
	_
	 |_
	   |_
	     |_
		   |_
		     |_

	*/
	const std::vector<int> heights = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	const std::size_t expected = 0;
	ASSERT_EQ(WaterCumulative(heights), expected);
}

TEST(WaterCumulativeTest, StairWithPits) {
	/*
	                  _
                  _  |
	          _  | |_|
	      ___| | |
	  _  |     |_|
	_| |_|

	*/
	const std::vector<int> heights = { 0, 1, 0, 2, 2, 3, 1, 4, 3, 5 };
	const std::size_t expected = 4;
	ASSERT_EQ(WaterCumulative(heights), expected);
}

TEST(WaterCumulativeTest, Difficult) {
	/*
	        _
	       | |_                   ___   _
	       |   |_     ___       _|   |_| |      ___      
	    _  |     |___|   |_____|         |_    |   |      _ 
	  _| |_|                               |___|   |    _| |_
    _|                                             |___|     |_
	*/
	const std::vector<int> heights = { 0, 1, 2, 1, 5, 4, 3, 2, 2, 3, 3, 2, 2, 2, 3, 4, 4, 3, 4, 2, 1, 1, 3, 3, 0, 0, 1, 2, 1, 0 };
	const std::size_t expected = 26;
	ASSERT_EQ(WaterCumulative(heights), expected);
}